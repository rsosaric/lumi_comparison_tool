# -*-*-*-*-*-*-*-*-*-*-*- > > > RAMSES calibration < < < -*-*-*-*-*-*-*-*-*-*-*-
ramses_output_file_name = "ramses_calibration_output/"
variation_studies_nbins = 50
to_ref_studies_nbins = 100
soft_ratio_lim = 10
medium_ratio_lim = 1.5
ramses_scale = 1.0e-07
ratio_to_ref_min_del = 1.64*ramses_scale
ratio_to_ref_max_del = 1.705*ramses_scale
ratio_to_ref_min_rec = 1.665*ramses_scale
ratio_to_ref_max_rec = 1.795*ramses_scale
ramses_cal_min_ratio = 0.90
ramses_cal_max_ratio = 1.10

# -*-*-*-*-*-*-*-*-*-*-*- > > > Configuration Backup < < < -*-*-*-*-*-*-*-*-*-*-*-

# # >>>>>>>>>>> 2017 2-channels-mean
# ramses_output_file_name = "ramses_calibration_output/"
# variation_studies_nbins = 50
# to_ref_studies_nbins = 100
# soft_ratio_lim = 10
# medium_ratio_lim = 1.5
# ramses_scale = 1.0e-07
# ratio_to_ref_min_del = 1.64*ramses_scale
# ratio_to_ref_max_del = 1.705*ramses_scale
# ratio_to_ref_min_rec = 1.665*ramses_scale
# ratio_to_ref_max_rec = 1.795*ramses_scale
# ramses_cal_min_ratio = 0.90
# ramses_cal_max_ratio = 1.10

# # >>>>>>>>>>> 2017 ch1
# ramses_output_file_name = "ramses_calibration_output/"
# variation_studies_nbins = 50
# to_ref_studies_nbins = 100
# soft_ratio_lim = 10
# medium_ratio_lim = 1.5
# ramses_scale = 1.0e-07
# ratio_to_ref_min_del = 1.8*ramses_scale
# ratio_to_ref_max_del = 2.1*ramses_scale
# ratio_to_ref_min_rec = 1.8*ramses_scale
# ratio_to_ref_max_rec = 2.2*ramses_scale
# ramses_cal_min_ratio = 0.90
# ramses_cal_max_ratio = 1.10

# # >>>>>>>>>>> 2017 ch2
# ramses_output_file_name = "ramses_calibration_output/"
# variation_studies_nbins = 50
# to_ref_studies_nbins = 100
# soft_ratio_lim = 10
# medium_ratio_lim = 1.5
# ramses_scale = 1.0e-07
# ratio_to_ref_min_del = 1.3*ramses_scale
# ratio_to_ref_max_del = 1.5*ramses_scale
# ratio_to_ref_min_rec = 1.3*ramses_scale
# ratio_to_ref_max_rec = 1.6*ramses_scale
# ramses_cal_min_ratio = 0.90
# ramses_cal_max_ratio = 1.10
